package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

func main() {
	// []string{"find", "dir=C:/Aaround", "--f", --name=test.txt", "--size=1", "--all"}
	// []string{"find", "dir=C:/Aaround", "--d", "--name=d30", "--size=0"}
	args := os.Args
	err, rootPath, by, isAll, findFile, findDir := parseRequest(args)
	fmt.Println("Find in : ", rootPath)
	fmt.Println("Find by : ", by)
	fmt.Println("Is All : ", isAll)
	fmt.Println("Find file : ", findFile)
	fmt.Println("Find dir : ", findDir)
	if err == nil {
		start(rootPath, by, isAll, findFile, findDir)
	}
}

func start(rootPath string, by map[string]string, isAll, findFile, findDir bool) {
	var (
		root   = createDirectory(&Directory{Path: rootPath}, nil)
		search = &Search{
			Root:       rootPath,
			FindBy:     by,
			FileSystem: root,
			FoundFiles: make(map[string]*File, 0),
			FoundDirs:  make(map[string]*Directory, 0),
			IsAll:      isAll,
			FindDir:    findDir,
			FindFile:   findFile,
		}
		handlers = make(MoreHandlers, 0)
	)
	handlers["--name"] = byName
	handlers["--size"] = bySize
	handlers["--cont"] = byContains
	find(search.FileSystem, search, handlers)
	fmt.Println("\n.............................................................................................\nResult\n")
	fmt.Println("\nFound files :")
	if len(search.FoundFiles) == 0 {
		fmt.Println("No such file exists.")
	} else {

		for path, file := range search.FoundFiles {
			fmt.Println("File ", file.Name, " ext. ", file.Ext, " in : ", path, " >> Size : ", file.Size, " >> Ext. : ", file.Ext)
		}
	}
	fmt.Println("\nFound dirs :")
	if len(search.FoundDirs) == 0 {
		fmt.Println("No such dir exists.")
	} else {

		for _, directory := range search.FoundDirs {
			var size int64 = 0
			for _, val := range directory.MapFiles {
				size += val.Size
			}
			fmt.Println("Dir ", directory.Path, " >> Size : ", size, " >> Count files : ", len(directory.MapFiles))
		}
	}
}

func find(directory *Directory, search *Search, moreHandlers MoreHandlers) {
	fmt.Println("Search in -> ", directory.Path)
	if directory.Path == search.Root && int64(len(directory.ListChild)) == directory.IndexNextChild {
		return
	}
	if performSearchFiles(directory, search, moreHandlers) {
		if !search.IsAll {
			return
		}
	}
	if int64(len(directory.ListChild)) == directory.IndexNextChild {
		find(directory.PointParent, search, moreHandlers)
		return
	}
	nextChild := createDirectory(directory.ListChild[directory.IndexNextChild], directory)
	directory.ListChild[directory.IndexNextChild].Check = true
	directory.IndexNextChild++
	if nextChild == nil {
		find(directory, search, moreHandlers)
	} else {
		find(nextChild, search, moreHandlers)
	}
}

func performSearchFiles(directory *Directory, search *Search, moreHandlers MoreHandlers) bool {
	var (
		resFile *File
		resPath = "resFile.Path"
	)
	for key, val := range search.FindBy {
		if isFind, file := activateHandler(directory, search, moreHandlers[key]); isFind == true {
			resFile = file
			resPath = file.Path
			if search.FindFile {
				fmt.Println("========> FILE IS FIND '", key, "' IN ", directory.Path, " WITH PARAM '", val, "'")
			} else {
				fmt.Println("========> DIR IS FIND '", key, "' IN ", directory.Path, " WITH PARAM '", val, "'")
			}
		} else {
			return false
		}
	}
	if search.FindFile {
		search.FoundFiles[resPath] = resFile
	} else {
		search.FoundDirs[directory.Path] = directory
	}
	return true
}

func activateHandler(directory *Directory, search *Search, handler Handler) (bool, *File) {
	for _, val := range directory.MapFiles {
		if handler(val, search, directory) {
			return true, val
		}
	}
	return false, nil
}

func createDirectory(directory *Directory, parent *Directory) *Directory {
	if content, err := getContentFromDir(directory.Path); err == nil {
		var (
			dirs  = make([]*Directory, 0)
			files = make(map[string]*File, 0)
		)
		for _, element := range content {
			if element.IsDir() {
				p := element.Name()
				if strings.Contains(p, ".") || strings.Contains(p, "$") {
					continue
				}
				dirs = append(dirs, &Directory{
					IndexNextChild: 0,
					Path:           directory.Path + element.Name() + "/",
					ListChild:      make([]*Directory, 0),
					MapFiles:       make(map[string]*File, 0),
					PointParent:    nil,
				})
			} else {
				n := element.Name()
				if strings.Contains(n, ".") {
					n = strings.Split(n, ".")[0]
				}
				p := directory.Path + element.Name()
				files[p] = &File{
					Size: element.Size(),
					Name: n,
					Path: p,
					Ext:  filepath.Ext(p),
				}
			}
		}
		directory.ListChild = dirs
		directory.PointParent = parent
		directory.MapFiles = files
		directory.IndexNextChild = 0
		return directory
	} else {
		fmt.Println(err)
		return nil
	}
}

func getContentFromDir(path string) ([]os.FileInfo, error) {
	content, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, err
	} else {
		return content, nil
	}
}

func parseRequest(args []string) (error, string, map[string]string, bool, bool, bool) {
	var (
		err      error
		rootPath = ""
		by       = make(map[string]string, 0)
		isAll    = false
		findFile = false
		findDir  = false
		add      = func(elem string) (error, string, string) {
			var e error
			if strings.Contains(elem, "=") {
				slice := strings.Split(elem, "=")
				return nil, slice[0], slice[1]
			} else {
				str := "Param '" + elem + "' must be contain '='"
				e = errors.New(str)
				return e, "", ""
			}
		}
	)
	if !strings.Contains(args[1], "dir") {
		err = errors.New("Root dir isn't exist ")
		return err, rootPath, by, isAll, findFile, findDir
	} else {
		if err, _, val := add(args[1]); err == nil {
			if string(val[len(val)-1]) != "/" {
				val += "/"
			}
			rootPath = val
		} else {
			return err, rootPath, by, isAll, findFile, findDir
		}
	}
	typ := args[2]
	if typ != "--f" {
		if typ != "--d" {
			err = errors.New("Type isn't exist ")
			return err, rootPath, by, isAll, findFile, findDir
		}
	}
	if args[2] == "--f" {
		findFile = true
	} else {
		findDir = true
	}
	for i := 3; i < len(args); i++ {
		if i == len(args)-1 {
			if args[len(args)-1] == "--all" {
				isAll = true
			} else {
				if e, key, val := add(args[i]); e == nil {
					by[key] = val
				} else {
					return err, rootPath, by, isAll, findFile, findDir
				}
			}
		} else {
			if e, key, val := add(args[i]); e == nil {
				by[key] = val
			} else {
				return err, rootPath, by, isAll, findFile, findDir
			}
		}
	}
	return err, rootPath, by, isAll, findFile, findDir
}
